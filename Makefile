
SUBDIRS = en he
TOPDIR := .

WDIR := $(TOPDIR)/shvilim-www
$(WDIR) $(WDIR)/images :
	mkdir -p $@
all : $(WDIR) $(WDIR)/images
	cp index.html main.css main.js $(WDIR)
	cp images/* $(WDIR)/images

install:
	tar cf - $(WDIR) | tar xf - -C /var/www/localhost/htdocs

install_g4:
	tar cf - $(WDIR) | \
		ssh -p 2974 root@192.168.2.101 tar xf - -C /shares/Public

include rules.mk
