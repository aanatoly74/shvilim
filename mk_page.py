#!/usr/bin/python

import re, sys

html = open("main.html", "r").read()
page = open(sys.argv[1], "r").read()

m = re.search('(?mis)<title>(.*)</title>', html)
title = m.group(1)

m = re.search('(?mis)<title>(.*)</title>', page)
title = m.group(1) + " - " + title

m = re.search('(?mis)<body>(.*)</body>', page)
content = m.group(1)

html = re.sub('(?mis)(?<=<title>).*?<', title + '<', html)
html = re.sub('(?mis)(<div id=.content.*?>)', '\\1<br/>' + content, html)

open(sys.argv[2], "w").write(html)

