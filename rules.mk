MAKEFLAGS += -rR #--no-print-directory
.DEFAULT_GOAL = all

.PHONY : all diff install $(SUBDIRS)
all diff install : $(SUBDIRS)

$(SUBDIRS):
	@$(MAKE) -C $@ $(MAKECMDGOALS)




ifeq ($(TOPDIR),)
A := $(dir $(word 1,$(foreach ud,.. ../.. ../../.. ../../../..,\
		$(wildcard $(ud)/rules.mk))))
B := $(if $A,$(shell realpath $A),$(CURDIR))/
# topdir holds rules.mk, so to find it - just climb up
TOPDIR:=$B
$(warning $(TOPDIR))
export TOPDIR
endif
CDIR=$(CURDIR)/
RDIR = $(subst $(TOPDIR),,$(CDIR))
WDIR = $(TOPDIR)/shvilim-www
all install : $(WDIR)

